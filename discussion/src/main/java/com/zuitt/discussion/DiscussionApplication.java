package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@PostMapping("/user")
	public String createUser (){
		return " New user created!";
	}

	@GetMapping("/user")
	public String getUser(){
		return "All user retrieved.";
	}

	@GetMapping("/user/{userid}")
	public String getUser(@PathVariable Long userid){
		return "Viewing details of user " +userid;

	}
	@DeleteMapping("/user/{userid}")
	public String deleteUser(@PathVariable Long userid,@RequestHeader(value = "Authorization")String user){
		String message;
		if(user.equals("")){
			message = "Not authorized";
		}
		else{
			message = "The user "+userid+" has been deleted";
		}
		return message;
	}
	@PutMapping("/user/{userid}")
	@ResponseBody
	public User updateUser(@PathVariable Long userid, @RequestBody User user){
		return user;
	}



}
